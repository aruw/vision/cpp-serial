#!/bin/sh

"""
Test script which imports cpp_serial and performs cursory validation.
"""

import sys

_, is_stub_str = sys.argv
expect_stub = { "true": True, "false": False }[is_stub_str.lower()]

print("BEGINNING SMOKE TEST ==========")

import cpp_serial
from cpp_serial import SerialByteProvider
from cpp_serial import BufferedSerialByteProvider

print(f"Module path: {cpp_serial.__path__}")
print(f"Is stub? {cpp_serial.__stub__}")

assert cpp_serial.__stub__ == expect_stub

# TODO: confirm functions exist

print("SMOKE TEST SUCCEEDED ==========")
