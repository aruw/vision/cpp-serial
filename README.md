# cpp-serial

## Build prerequisites

```
pip install scikit-build wheel ninja
sudo apt install cmake gcc-9
sudo apt install libboost-system-dev
```

## Building a wheel

```bash
git submodule update --init --recursive
python3 setup.py clean
rm -r _skbuild build dist

# "Real" native module for current platform:
python3 setup.py bdist_wheel

# Pure Python multi-platform stub package:
CPP_SERIAL_BUILD_STUB= python3 setup.py bdist_wheel
```

Built wheel(s) will be in the `dist/` subdirectory.
