try:
    from ._cpp_serial import SerialByteProvider, BufferedSerialByteProvider
    __stub__ = False
except ImportError:
    from ._unsupported_platform_shim import SerialByteProvider, BufferedSerialByteProvider
    __stub__ = True
