NOT_SUPPORTED_MESSAGE = "cpp_serial is not supported on this platform."

class DummyByteProvider:
    def read_byte(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

    def read_bytes(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

    def write_byte(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

    def write_bytes(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

    def consume_overflow_status(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

    def in_waiting(self, *args, **kwargs):
        raise NotImplementedError(NOT_SUPPORTED_MESSAGE)

SerialByteProvider = DummyByteProvider
BufferedSerialByteProvider = DummyByteProvider
