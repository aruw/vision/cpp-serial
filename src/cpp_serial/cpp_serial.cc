#include <buffered_byte_provider.h>
#include <py_wrapper.h>
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <serial_byte_provider.h>

namespace py = pybind11;

PYBIND11_MODULE(_cpp_serial, m) {
  using serial_byte_provider = wrapper::py_wrapper<provider::serial_byte_provider>;
  py::class_<serial_byte_provider>(m, "SerialByteProvider")
      .def(py::init<const std::string&, const size_t&>())
      .def("read_byte", &serial_byte_provider::read_byte)
      .def("read_bytes", &serial_byte_provider::read_bytes)
      .def("write_byte", &serial_byte_provider::write_byte)
      .def("write_bytes", &serial_byte_provider::write_bytes)
      .def("consume_overflow_status", &serial_byte_provider::consume_overflow_status)
      .def("in_waiting", &serial_byte_provider::in_waiting);


  using buffered_serial_byte_provider = wrapper::py_wrapper<provider::buffered_byte_provider<provider::serial_byte_provider>>;
  py::class_<buffered_serial_byte_provider>(m, "BufferedSerialByteProvider")
      .def(py::init<const size_t&, const std::string&, const size_t&>())
      .def("read_byte", &buffered_serial_byte_provider::read_byte)
      .def("read_bytes", &buffered_serial_byte_provider::read_bytes)
      .def("write_byte", &buffered_serial_byte_provider::write_byte)
      .def("write_bytes", &buffered_serial_byte_provider::write_bytes)
      .def("consume_overflow_status", &buffered_serial_byte_provider::consume_overflow_status)
      .def("in_waiting", &buffered_serial_byte_provider::in_waiting);
}