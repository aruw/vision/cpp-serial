import os

__version__ = "1.1.1"

STUB_ENV_VAR_NAME = "CPP_SERIAL_BUILD_STUB"
build_stub = STUB_ENV_VAR_NAME in os.environ

base_setup_options = {
    "name": "cpp_serial",
    "version": __version__,
    "author": "Advanced Robotics at the University of Washington",
    "author_email": "robomstr@uw.edu",
    "description": "A native module exposing predictable-latency serial port operations.",
    "long_description": "",
    "zip_safe": False,
    "packages": ['cpp_serial'],
    "package_data": {'cpp_serial': ['__init__.pyi']},
    "package_dir": {'': 'src'},
    "python_requires": ">=3.8",
}

additional_native_setup_options = {
    "cmake_install_dir": 'src/cpp_serial',
}

if build_stub:
    from setuptools import setup

    setup(**base_setup_options)
else:
    from skbuild import setup

    setup(
        **base_setup_options,
        **additional_native_setup_options,
    )
