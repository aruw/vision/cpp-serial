#pragma once

#include <pybind11/pybind11.h>

#include <string>

namespace provider {

using byte_type = char;
using byte_array_type = std::string;

static_assert(sizeof(byte_type) == 1);

}  // namespace provider