#pragma once

#include <byte_provider_types.h>

#include <boost/asio.hpp>
#include <cstdint>
#include <mutex>
#include <vector>

namespace provider {

struct serial_byte_provider {
  std::mutex read_mutex_{};
  std::mutex write_mutex_{};

  boost::asio::io_service io_;

  boost::asio::serial_port read_port_;
  boost::asio::serial_port write_port_;

  byte_type read_byte() {
    std::lock_guard lock_(read_mutex_);
    byte_type result{};
    boost::asio::read(read_port_, boost::asio::buffer(&result, sizeof(byte_type)));
    return result;
  }

  byte_array_type read_bytes(const size_t& n) {
    std::lock_guard lock_(read_mutex_);
    byte_array_type result(n, byte_type{});
    boost::asio::read(read_port_, boost::asio::buffer(result.data(), result.size()));
    return result;
  }

  void write_byte(const byte_type& data) {
    std::lock_guard lock_(write_mutex_);
    boost::asio::write(write_port_, boost::asio::buffer(&data, sizeof(byte_type)));
  }

  void write_bytes(const byte_array_type& data) {
    std::lock_guard lock_(write_mutex_);
    boost::asio::write(write_port_, boost::asio::buffer(data.data(), data.size()));
  }

  constexpr bool consume_overflow_status() const { return false; }
  constexpr size_t in_waiting() const { return 0; }

  serial_byte_provider(const std::string& port_name, const size_t& baud_rate) : io_{}, read_port_{io_}, write_port_{io_} {
    read_port_.open(port_name);
    write_port_.open(port_name);
    read_port_.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
    write_port_.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
  }
};

}  // namespace provider
