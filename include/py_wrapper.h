#pragma once

#include <byte_provider_types.h>
#include <pybind11/pybind11.h>

#include <cstdint>
#include <utility>
#include <vector>

namespace wrapper {

using py_byte_list_type = std::vector<std::uint8_t>;

template <typename SourceByteProvider>
struct py_wrapper {
  SourceByteProvider provider_;

  provider::byte_type read_byte() {
    pybind11::gil_scoped_release release;
    return provider_.read_byte();
  }

  py_byte_list_type read_bytes(const size_t& n) {
    pybind11::gil_scoped_release release;
    const provider::byte_array_type bytes = provider_.read_bytes(n);
    return py_byte_list_type(bytes.begin(), bytes.end());
  }

  void write_byte(const provider::byte_type& data) {
    pybind11::gil_scoped_release release;
    provider_.write_byte(data);
  }

  void write_bytes(const provider::byte_array_type& data) {
    pybind11::gil_scoped_release release;
    provider_.write_bytes(data);
  }

  bool consume_overflow_status() {
    pybind11::gil_scoped_release release;
    return provider_.consume_overflow_status();
  }

  size_t in_waiting() {
    pybind11::gil_scoped_release release;
    return provider_.in_waiting();
  }

  template <typename... Ts>
  py_wrapper(Ts&&... ts) : provider_{std::forward<Ts>(ts)...} {}
};

}  // namespace wrapper