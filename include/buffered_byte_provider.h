#pragma once

#include <byte_provider_types.h>

#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <deque>
#include <mutex>
#include <thread>
#include <utility>
#include <vector>

namespace provider {

template <typename SourceByteProvider>
struct buffered_byte_provider {
  SourceByteProvider provider_;

  bool overflow_status_{false};
  size_t capacity_;
  std::deque<byte_type> buffer_;

  std::atomic_bool kill_{false};
  std::mutex buffer_mutex_{};
  std::condition_variable cv_{};

  std::thread loop_thread_;

  void loop_() {
    while (!kill_.load(std::memory_order::memory_order_relaxed)) {
      const byte_type byte = provider_.read_byte();

      {
        std::lock_guard lock_(buffer_mutex_);
        if (buffer_.size() >= capacity_) {
          buffer_.clear();
          overflow_status_ = true;
        }
        buffer_.push_back(byte);
      }

      cv_.notify_one();
    }
  }

  byte_type read_byte() { return *read_bytes(sizeof(byte_type)).begin(); }

  byte_array_type read_bytes(const size_t& n) {
    byte_array_type result{};

    std::unique_lock lock_(buffer_mutex_);
    cv_.wait(lock_, [&, this] {
      while (!buffer_.empty() && result.size() < n) {
        result.push_back(buffer_.front());
        buffer_.pop_front();
      }

      return result.size() >= n;
    });

    return result;
  }

  void write_byte(const byte_type& data) { provider_.write_byte(data); }
  void write_bytes(const byte_array_type& data) { provider_.write_bytes(data); }

  bool consume_overflow_status() {
    std::lock_guard lock_(buffer_mutex_);
    const bool overflow_status = overflow_status_;
    overflow_status_ = false;
    return overflow_status;
  }

  size_t in_waiting() {
    std::lock_guard lock_(buffer_mutex_);
    return buffer_.size();
  }

  template <typename... Ts>
  buffered_byte_provider(const size_t& capacity, Ts&&... ts)
      : provider_{std::forward<Ts>(ts)...}, capacity_{capacity}, buffer_{}, loop_thread_([this] { loop_(); }) {}

  ~buffered_byte_provider() {
    kill_.store(true, std::memory_order::memory_order_relaxed);
    loop_thread_.join();
  }
};

}  // namespace provider