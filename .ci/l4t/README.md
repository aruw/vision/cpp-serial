# cpp-serial arm64 (Linux4Tegra) CI image

## Building and publishing the container image

_Make sure you have `jq` installed. On Ubuntu: `sudo apt install jq`._



First, make your Dockerfile changes and build the new image:
```bash
make build
```

The image is now locally tagged as `aruw/cpp-serial-arm64-l4t-ci:latest`. If it works like you expect, login to the GitLab container registry (you will need to generate a personal access token with read and write registry scopes if you have 2FA enabled on your GitLab account) and publish
the new image:

```bash
docker login registry.gitlab.com
make publish
```

Images are versioned by the date they were created, with an incrementing numeric suffix if there are
multiple builds on the same day. For example, `2020-08-25.3`.

`make publish` will automatically pick up the current date from your local system clock and appropriate
suffix from GitLab Package Repository.
